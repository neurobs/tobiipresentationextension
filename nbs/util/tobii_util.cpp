#include "nbs/util/tobii_util.h"

#include <codecvt>

using namespace nbs;
using namespace nbs::util;

std::wstring util::utf8_to_utf16(const std::string& s)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t> > converter;
	return converter.from_bytes(s);
}

std::string nbs::util::utf16_to_utf8(const std::wstring & s)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
	return converter.to_bytes(s);
}

std::vector<Eye_tracker_list::Info> Eye_tracker_list::get_info()
{
	std::vector<Info> list;
	auto data = trackers_.data();
	for (size_t i = 0; i < data->count; i++)
	{
		auto eyetracker = data->eyetrackers[i];

		Tobii_string address;
		check_call(tobii_research_get_address(eyetracker, address.assignee()), "Error getting eye tracker address.");

		Tobii_string serial_number;
		check_call(tobii_research_get_serial_number(eyetracker, serial_number.assignee()), "Error getting eye tracker serial number.");

		Tobii_string device_name;
		check_call(tobii_research_get_device_name(eyetracker, device_name.assignee()), "Error getting eye tracker device name.");

		list.push_back({ address.data(), serial_number.data(), device_name.data() });
	}
	return list;
}