#pragma once

#include <deque>
#include <atomic>
#include <thread>
#include <mutex>

namespace nbs
{

namespace util
{

/// Thread-safe deque that uses a mutex lock
template <typename T>
class Locked_deque
{
public:
	/// \param max_size If the vector reaches this size, 
	/// additional elements pushed will be ignored.
	Locked_deque(size_t max_size)
		:
		maximum_size_(max_size),
		size_(0)
	{}

	/// \brief Locks the vector and pushes an item
	void push(const T& t)
	{
		std::lock_guard<std::mutex> guard(mutex_);
		if (queue_.size() < maximum_size_)
		{
			queue_.push_back(t);
			size_.store(queue_.size());
		}
	}

	/// \brief Locks the vector and pushes an item
	void push(T&& t)
	{
		std::lock_guard<std::mutex> guard(mutex_);
		if (queue_.size() < maximum_size_)
		{
			queue_.push_back(std::move(t));
			size_.store(queue_.size());
		}
	}

	/// \brief Copies elements and removes them from the 
	/// front of the queue
	///
	/// \param buffer_size Count of T's to copy and remove
	///
	/// \return Count of T's copied and removed
	size_t retrieve(T* buffer, size_t buffer_size)
	{
		size_t copied = 0;
		if (!empty())
		{
			std::lock_guard<std::mutex> guard(mutex_);
			copied = (buffer_size < size_) ? buffer_size : size_;
			auto begin = queue_.begin();
			auto end = queue_.begin() + copied;
			std::copy(begin, end, buffer);
			queue_.erase(begin, end);
			size_.store(queue_.size());
		}
		return copied;
	}

	/// \brief Returns all items and clears the vector
	std::deque<T> retrieve()
	{
		std::deque<T> values;
		if (!empty())
		{
			std::lock_guard<std::mutex> guard(mutex_);
			queue_.swap( values );
			size_.store(0);
		}
		return values;
	}

	void clear()
	{
		if (!empty())
		{
			std::lock_guard<std::mutex> guard(mutex_);
			queue_.clear();
			size_.store( 0 );
		}
	}

	size_t size() const { return size_.load(); }
	bool empty() const { return size() == 0; }

	Locked_deque(const Locked_deque&) = delete;
	void operator=(const Locked_deque&) = delete;

private:
	std::deque<T> queue_;
	std::atomic<size_t> size_;
	const size_t maximum_size_;
	mutable std::mutex mutex_;
};

} // namespace util

} // namespace nbs















