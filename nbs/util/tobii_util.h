#pragma once

#include <string>
#include <vector>
#include <exception>
extern "C"
{
	#include "tobii_research.h"
	#include "tobii_research_eyetracker.h"
	#include "tobii_research_streams.h"
	#include "tobii_research_calibration.h"
}

namespace nbs
{

namespace util
{

std::wstring utf8_to_utf16(const std::string& s);
std::string utf16_to_utf8(const std::wstring& s);

/// \brief Screen resolution in pixels
struct Screen_size
{
	long width_;
	long height_;
};

/// Location in pixels in the Presentation coordinate system -
/// origin center, x positive right, y positive up
struct Presentation_point
{
	long x_;
	long y_;
};

inline Presentation_point convert_point(TobiiResearchNormalizedPoint2D tp, Screen_size size)
{
	Presentation_point pp;
	pp.x_ = static_cast<long>((tp.x - 0.5) * size.width_);
	pp.y_ = static_cast<long>((0.5 - tp.y) * size.height_);
	return pp;
}

inline TobiiResearchNormalizedPoint2D convert_point(Presentation_point pp, Screen_size size)
{
	TobiiResearchNormalizedPoint2D tp;
	tp.x = static_cast<float>(pp.x_) / size.width_ + 0.5f;
	tp.y = 0.5f - static_cast<float>(pp.y_) / size.height_;
	return tp;
}

struct Tobii_exception : public std::exception
{
	Tobii_exception(std::string message, TobiiResearchStatus status)
		:
		message_(message + "\nThe status code was " + std::to_string(status)),
		status_(status)
	{}
	const std::string message_;
	const TobiiResearchStatus status_;

	virtual char const* what() const override { return message_.c_str(); }
};

inline void check_call(TobiiResearchStatus status, const char* message)
{
	if (status != TOBII_RESEARCH_STATUS_OK)
	{
		throw Tobii_exception(message, status);
	}
}

inline void destroy_tobii_resource(char* data) { tobii_research_free_string(data); }
inline void destroy_tobii_resource(TobiiResearchCalibrationResult* data) { tobii_research_free_screen_based_calibration_result(data); }
inline void destroy_tobii_resource(TobiiResearchEyeTrackers* data) { tobii_research_free_eyetrackers(data); }

template <typename T>
class Tobii_resource
{
public:
	Tobii_resource() : data_(nullptr) {}
	~Tobii_resource() { clear(); }

	const T* data() const
	{
		if (!data_)
		{
			throw std::logic_error("Accessing Tobii_resource before initialization");
		}
		return data_;
	}
	T** assignee() { clear(); return &data_; }

	Tobii_resource(const Tobii_resource&) = delete;
	void operator=(const Tobii_resource&) = delete;

private:
	T* data_;

	void clear()
	{
		if (data_)
		{
			destroy_tobii_resource(data_);
			data_ = nullptr;
		}
	}
};

typedef Tobii_resource<char> Tobii_string;
typedef Tobii_resource<TobiiResearchCalibrationResult> Tobii_calibration_result;
typedef Tobii_resource<TobiiResearchEyeTrackers> Tobii_eye_trackers;

class Eye_tracker_list
{
public:
	Eye_tracker_list()
	{
		check_call(tobii_research_find_all_eyetrackers(trackers_.assignee()), "Error getting available eye trackers.");
	}

	struct Info
	{
		std::string address_;
		std::string serial_number_;
		std::string device_name_;
	};
	std::vector<Info> get_info();

private:
	Tobii_eye_trackers trackers_;
};

}

}
