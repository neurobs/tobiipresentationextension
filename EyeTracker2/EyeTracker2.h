

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Wed Jun 04 15:20:31 2008
 */
/* Compiler settings for EyeTracker2.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __EyeTracker2_h__
#define __EyeTracker2_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IEyeTracker2_FWD_DEFINED__
#define __IEyeTracker2_FWD_DEFINED__
typedef interface IEyeTracker2 IEyeTracker2;
#endif 	/* __IEyeTracker2_FWD_DEFINED__ */


#ifndef __IEyeTracker2_FWD_DEFINED__
#define __IEyeTracker2_FWD_DEFINED__
typedef interface IEyeTracker2 IEyeTracker2;
#endif 	/* __IEyeTracker2_FWD_DEFINED__ */


/* header files for imported files */
#include "unknwn.h"
#include "oaidl.h"
#include "EyeTracker.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_EyeTracker2_0000 */
/* [local] */ 

typedef /* [uuid] */  DECLSPEC_UUID("2a384a38-8662-4d84-addc-e4087e295b37") struct EyeTracker2GeneralData
    {
    long type;
    long subType;
    long eye;
    unsigned __int64 time;
    double xData;
    double yData;
    double data1;
    double data2;
    double data3;
    } 	EyeTracker2GeneralData;

typedef /* [uuid] */  DECLSPEC_UUID("53fde6fc-c46e-40ea-b3b8-416ae558ec07") 
enum tagEyeTracker2EyeType
    {	ET2_ET_DEFAULT	= 0,
	ET2_ET_RIGHT	= 1,
	ET2_ET_LEFT	= 2,
	ET2_ET_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2EyeType;

typedef /* [uuid] */  DECLSPEC_UUID("9abb62af-7959-4ba7-845c-21a2bd443f4c") 
enum tagEyeTracker2DataType
    {	ET2_DT_POSITION	= 0,
	ET2_DT_SACCADE	= 1,
	ET2_DT_FIXATION	= 2,
	ET2_DT_AOI	= 3,
	ET2_DT_PUPIL	= 4,
	ET2_DT_BLINK	= 5,
	ET2_DT_TRIGGER	= 6,
	ET2_DT_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2DataType;

typedef /* [uuid] */  DECLSPEC_UUID("64d1074c-0bca-4364-aac9-146b01200d8b") 
enum tagEyeTracker2CalibrationType
    {	ET2_CT_CALIBRATE	= 0,
	ET2_CT_DRIFT_CORRECT	= 1,
	ET2_CT_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2CalibrationType;



extern RPC_IF_HANDLE __MIDL_itf_EyeTracker2_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_EyeTracker2_0000_v0_0_s_ifspec;

#ifndef __IEyeTracker2_INTERFACE_DEFINED__
#define __IEyeTracker2_INTERFACE_DEFINED__

/* interface IEyeTracker2 */
/* [object][oleautomation][dual][version][uuid] */ 


EXTERN_C const IID IID_IEyeTracker2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1b33d185-1217-403b-bb78-ca99b276ecdd")
    IEyeTracker2 : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT __stdcall GetCaps( 
            /* [retval][out] */ long *caps) = 0;
        
        virtual /* [id] */ HRESULT __stdcall IsMultithreaded( 
            /* [retval][out] */ VARIANT_BOOL *multiThreaded) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetDisplayMode( 
            /* [in] */ long width,
            /* [in] */ long height) = 0;
        
        virtual /* [id] */ HRESULT __stdcall Execute( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetTime( 
            /* [retval][out] */ unsigned __int64 *time) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetStatus( 
            /* [retval][out] */ long *status) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetLastError( 
            /* [retval][out] */ BSTR *message) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetEventCount( 
            /* [retval][out] */ long *count) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetEvents( 
            /* [out][in] */ long *count,
            /* [out] */ EyeTracker2GeneralData *events) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartData( 
            /* [in] */ EyeTracker2DataType type,
            /* [in] */ EyeTracker2EyeType eye,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopData( 
            /* [in] */ EyeTracker2DataType type,
            /* [in] */ EyeTracker2EyeType eye,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetTracking( 
            /* [in] */ VARIANT_BOOL trackingOn,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetRecording( 
            /* [in] */ VARIANT_BOOL recordingOn,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall Calibrate( 
            /* [in] */ EyeTracker2CalibrationType calibrationType,
            /* [in] */ ICalibrationDisplay *interfacePtr,
            /* [in] */ double parameter1,
            /* [in] */ double parameter2,
            /* [in] */ double parameter3,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartCalibration( 
            /* [in] */ long pointCount,
            /* [in] */ const long *points,
            /* [in] */ EyeTracker2EyeType eye,
            /* [in] */ LPCWSTR parameters,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetCalibrationPoint( 
            /* [out] */ long *pointIndex,
            /* [out] */ long *x,
            /* [out] */ long *y,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall AcceptPoint( 
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopCalibration( 
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SendTrigger( 
            /* [in] */ long code) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SendString( 
            /* [in] */ LPCWSTR message) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SendCommand( 
            /* [in] */ LPCWSTR message,
            /* [retval][out] */ long *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SendMessage( 
            /* [in] */ LPCWSTR message,
            /* [retval][out] */ BSTR *response) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetParameter( 
            /* [in] */ LPCWSTR name,
            /* [in] */ LPCWSTR value,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetParameter( 
            /* [in] */ LPCWSTR name,
            /* [retval][out] */ BSTR *value) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetAOISet( 
            /* [in] */ long id,
            /* [retval][out] */ BSTR *returnValue) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEyeTracker2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEyeTracker2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEyeTracker2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEyeTracker2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEyeTracker2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEyeTracker2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEyeTracker2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEyeTracker2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( __stdcall *GetCaps )( 
            IEyeTracker2 * This,
            /* [retval][out] */ long *caps);
        
        /* [id] */ HRESULT ( __stdcall *IsMultithreaded )( 
            IEyeTracker2 * This,
            /* [retval][out] */ VARIANT_BOOL *multiThreaded);
        
        /* [id] */ HRESULT ( __stdcall *SetDisplayMode )( 
            IEyeTracker2 * This,
            /* [in] */ long width,
            /* [in] */ long height);
        
        /* [id] */ HRESULT ( __stdcall *Execute )( 
            IEyeTracker2 * This);
        
        /* [id] */ HRESULT ( __stdcall *GetTime )( 
            IEyeTracker2 * This,
            /* [retval][out] */ unsigned __int64 *time);
        
        /* [id] */ HRESULT ( __stdcall *GetStatus )( 
            IEyeTracker2 * This,
            /* [retval][out] */ long *status);
        
        /* [id] */ HRESULT ( __stdcall *GetLastError )( 
            IEyeTracker2 * This,
            /* [retval][out] */ BSTR *message);
        
        /* [id] */ HRESULT ( __stdcall *GetEventCount )( 
            IEyeTracker2 * This,
            /* [retval][out] */ long *count);
        
        /* [id] */ HRESULT ( __stdcall *GetEvents )( 
            IEyeTracker2 * This,
            /* [out][in] */ long *count,
            /* [out] */ EyeTracker2GeneralData *events);
        
        /* [id] */ HRESULT ( __stdcall *StartData )( 
            IEyeTracker2 * This,
            /* [in] */ EyeTracker2DataType type,
            /* [in] */ EyeTracker2EyeType eye,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *StopData )( 
            IEyeTracker2 * This,
            /* [in] */ EyeTracker2DataType type,
            /* [in] */ EyeTracker2EyeType eye,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *SetTracking )( 
            IEyeTracker2 * This,
            /* [in] */ VARIANT_BOOL trackingOn,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *SetRecording )( 
            IEyeTracker2 * This,
            /* [in] */ VARIANT_BOOL recordingOn,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *Calibrate )( 
            IEyeTracker2 * This,
            /* [in] */ EyeTracker2CalibrationType calibrationType,
            /* [in] */ ICalibrationDisplay *interfacePtr,
            /* [in] */ double parameter1,
            /* [in] */ double parameter2,
            /* [in] */ double parameter3,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *StartCalibration )( 
            IEyeTracker2 * This,
            /* [in] */ long pointCount,
            /* [in] */ const long *points,
            /* [in] */ EyeTracker2EyeType eye,
            /* [in] */ LPCWSTR parameters,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *GetCalibrationPoint )( 
            IEyeTracker2 * This,
            /* [out] */ long *pointIndex,
            /* [out] */ long *x,
            /* [out] */ long *y,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *AcceptPoint )( 
            IEyeTracker2 * This,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *StopCalibration )( 
            IEyeTracker2 * This,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *SendTrigger )( 
            IEyeTracker2 * This,
            /* [in] */ long code);
        
        /* [id] */ HRESULT ( __stdcall *SendString )( 
            IEyeTracker2 * This,
            /* [in] */ LPCWSTR message);
        
        /* [id] */ HRESULT ( __stdcall *SendCommand )( 
            IEyeTracker2 * This,
            /* [in] */ LPCWSTR message,
            /* [retval][out] */ long *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *SendMessage )( 
            IEyeTracker2 * This,
            /* [in] */ LPCWSTR message,
            /* [retval][out] */ BSTR *response);
        
        /* [id] */ HRESULT ( __stdcall *SetParameter )( 
            IEyeTracker2 * This,
            /* [in] */ LPCWSTR name,
            /* [in] */ LPCWSTR value,
            /* [retval][out] */ BSTR *returnValue);
        
        /* [id] */ HRESULT ( __stdcall *GetParameter )( 
            IEyeTracker2 * This,
            /* [in] */ LPCWSTR name,
            /* [retval][out] */ BSTR *value);
        
        /* [id] */ HRESULT ( __stdcall *SetAOISet )( 
            IEyeTracker2 * This,
            /* [in] */ long id,
            /* [retval][out] */ BSTR *returnValue);
        
        END_INTERFACE
    } IEyeTracker2Vtbl;

    interface IEyeTracker2
    {
        CONST_VTBL struct IEyeTracker2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEyeTracker2_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEyeTracker2_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEyeTracker2_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEyeTracker2_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEyeTracker2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEyeTracker2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEyeTracker2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEyeTracker2_GetCaps(This,caps)	\
    (This)->lpVtbl -> GetCaps(This,caps)

#define IEyeTracker2_IsMultithreaded(This,multiThreaded)	\
    (This)->lpVtbl -> IsMultithreaded(This,multiThreaded)

#define IEyeTracker2_SetDisplayMode(This,width,height)	\
    (This)->lpVtbl -> SetDisplayMode(This,width,height)

#define IEyeTracker2_Execute(This)	\
    (This)->lpVtbl -> Execute(This)

#define IEyeTracker2_GetTime(This,time)	\
    (This)->lpVtbl -> GetTime(This,time)

#define IEyeTracker2_GetStatus(This,status)	\
    (This)->lpVtbl -> GetStatus(This,status)

#define IEyeTracker2_GetLastError(This,message)	\
    (This)->lpVtbl -> GetLastError(This,message)

#define IEyeTracker2_GetEventCount(This,count)	\
    (This)->lpVtbl -> GetEventCount(This,count)

#define IEyeTracker2_GetEvents(This,count,events)	\
    (This)->lpVtbl -> GetEvents(This,count,events)

#define IEyeTracker2_StartData(This,type,eye,returnValue)	\
    (This)->lpVtbl -> StartData(This,type,eye,returnValue)

#define IEyeTracker2_StopData(This,type,eye,returnValue)	\
    (This)->lpVtbl -> StopData(This,type,eye,returnValue)

#define IEyeTracker2_SetTracking(This,trackingOn,returnValue)	\
    (This)->lpVtbl -> SetTracking(This,trackingOn,returnValue)

#define IEyeTracker2_SetRecording(This,recordingOn,returnValue)	\
    (This)->lpVtbl -> SetRecording(This,recordingOn,returnValue)

#define IEyeTracker2_Calibrate(This,calibrationType,interfacePtr,parameter1,parameter2,parameter3,returnValue)	\
    (This)->lpVtbl -> Calibrate(This,calibrationType,interfacePtr,parameter1,parameter2,parameter3,returnValue)

#define IEyeTracker2_StartCalibration(This,pointCount,points,eye,parameters,returnValue)	\
    (This)->lpVtbl -> StartCalibration(This,pointCount,points,eye,parameters,returnValue)

#define IEyeTracker2_GetCalibrationPoint(This,pointIndex,x,y,returnValue)	\
    (This)->lpVtbl -> GetCalibrationPoint(This,pointIndex,x,y,returnValue)

#define IEyeTracker2_AcceptPoint(This,returnValue)	\
    (This)->lpVtbl -> AcceptPoint(This,returnValue)

#define IEyeTracker2_StopCalibration(This,returnValue)	\
    (This)->lpVtbl -> StopCalibration(This,returnValue)

#define IEyeTracker2_SendTrigger(This,code)	\
    (This)->lpVtbl -> SendTrigger(This,code)

#define IEyeTracker2_SendString(This,message)	\
    (This)->lpVtbl -> SendString(This,message)

#define IEyeTracker2_SendCommand(This,message,returnValue)	\
    (This)->lpVtbl -> SendCommand(This,message,returnValue)

#define IEyeTracker2_SendMessage(This,message,response)	\
    (This)->lpVtbl -> SendMessage(This,message,response)

#define IEyeTracker2_SetParameter(This,name,value,returnValue)	\
    (This)->lpVtbl -> SetParameter(This,name,value,returnValue)

#define IEyeTracker2_GetParameter(This,name,value)	\
    (This)->lpVtbl -> GetParameter(This,name,value)

#define IEyeTracker2_SetAOISet(This,id,returnValue)	\
    (This)->lpVtbl -> SetAOISet(This,id,returnValue)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT __stdcall IEyeTracker2_GetCaps_Proxy( 
    IEyeTracker2 * This,
    /* [retval][out] */ long *caps);


void __RPC_STUB IEyeTracker2_GetCaps_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_IsMultithreaded_Proxy( 
    IEyeTracker2 * This,
    /* [retval][out] */ VARIANT_BOOL *multiThreaded);


void __RPC_STUB IEyeTracker2_IsMultithreaded_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SetDisplayMode_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ long width,
    /* [in] */ long height);


void __RPC_STUB IEyeTracker2_SetDisplayMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_Execute_Proxy( 
    IEyeTracker2 * This);


void __RPC_STUB IEyeTracker2_Execute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_GetTime_Proxy( 
    IEyeTracker2 * This,
    /* [retval][out] */ unsigned __int64 *time);


void __RPC_STUB IEyeTracker2_GetTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_GetStatus_Proxy( 
    IEyeTracker2 * This,
    /* [retval][out] */ long *status);


void __RPC_STUB IEyeTracker2_GetStatus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_GetLastError_Proxy( 
    IEyeTracker2 * This,
    /* [retval][out] */ BSTR *message);


void __RPC_STUB IEyeTracker2_GetLastError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_GetEventCount_Proxy( 
    IEyeTracker2 * This,
    /* [retval][out] */ long *count);


void __RPC_STUB IEyeTracker2_GetEventCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_GetEvents_Proxy( 
    IEyeTracker2 * This,
    /* [out][in] */ long *count,
    /* [out] */ EyeTracker2GeneralData *events);


void __RPC_STUB IEyeTracker2_GetEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_StartData_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ EyeTracker2DataType type,
    /* [in] */ EyeTracker2EyeType eye,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_StartData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_StopData_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ EyeTracker2DataType type,
    /* [in] */ EyeTracker2EyeType eye,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_StopData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SetTracking_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ VARIANT_BOOL trackingOn,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_SetTracking_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SetRecording_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ VARIANT_BOOL recordingOn,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_SetRecording_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_Calibrate_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ EyeTracker2CalibrationType calibrationType,
    /* [in] */ ICalibrationDisplay *interfacePtr,
    /* [in] */ double parameter1,
    /* [in] */ double parameter2,
    /* [in] */ double parameter3,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_Calibrate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_StartCalibration_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ long pointCount,
    /* [in] */ const long *points,
    /* [in] */ EyeTracker2EyeType eye,
    /* [in] */ LPCWSTR parameters,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_StartCalibration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_GetCalibrationPoint_Proxy( 
    IEyeTracker2 * This,
    /* [out] */ long *pointIndex,
    /* [out] */ long *x,
    /* [out] */ long *y,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_GetCalibrationPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_AcceptPoint_Proxy( 
    IEyeTracker2 * This,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_AcceptPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_StopCalibration_Proxy( 
    IEyeTracker2 * This,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_StopCalibration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SendTrigger_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ long code);


void __RPC_STUB IEyeTracker2_SendTrigger_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SendString_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ LPCWSTR message);


void __RPC_STUB IEyeTracker2_SendString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SendCommand_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ LPCWSTR message,
    /* [retval][out] */ long *returnValue);


void __RPC_STUB IEyeTracker2_SendCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SendMessage_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ LPCWSTR message,
    /* [retval][out] */ BSTR *response);


void __RPC_STUB IEyeTracker2_SendMessage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SetParameter_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ LPCWSTR name,
    /* [in] */ LPCWSTR value,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_SetParameter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_GetParameter_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ LPCWSTR name,
    /* [retval][out] */ BSTR *value);


void __RPC_STUB IEyeTracker2_GetParameter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker2_SetAOISet_Proxy( 
    IEyeTracker2 * This,
    /* [in] */ long id,
    /* [retval][out] */ BSTR *returnValue);


void __RPC_STUB IEyeTracker2_SetAOISet_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEyeTracker2_INTERFACE_DEFINED__ */



#ifndef __EyeTracker2Lib_LIBRARY_DEFINED__
#define __EyeTracker2Lib_LIBRARY_DEFINED__

/* library EyeTracker2Lib */
/* [uuid] */ 

typedef /* [uuid] */  DECLSPEC_UUID("eb289ae4-4344-40a0-8c1b-aa4f4dff5c50") 
enum tagEyeTracker2CapabilityFlags
    {	ET2_SUPPORTS_POSITION	= 1,
	ET2_SUPPORTS_PUPIL	= 2,
	ET2_SUPPORTS_FIXATION	= 4,
	ET2_SUPPORTS_SACCADE	= 8,
	ET2_SUPPORTS_AOI	= 16,
	ET2_SUPPORTS_BLINK	= 32,
	ET2_SUPPORTS_MULTIPLE_EYES	= 64,
	ET2_CAPABILITY_FLAGS_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2CapabilityFlags;

typedef /* [uuid] */  DECLSPEC_UUID("fe19454d-dc66-4599-aad6-28cc04cbf2ab") 
enum tagEyeTracker2Status
    {	ET2_STATUS_TRACKING_LEFT	= 1,
	ET2_STATUS_TRACKING_RIGHT	= 2,
	ET2_STATUS_TRACKING	= 4,
	ET2_STATUS_RECORDING	= 8,
	ET2_STATUS_STOPPED	= 16,
	ET2_STATUS_INITIALIZING	= 32,
	ET2_STATUS_ERROR	= 64,
	ET2_STATUS_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2Status;

typedef /* [uuid] */  DECLSPEC_UUID("cdad7eae-485f-4bd2-aa7a-a9374cde884c") 
enum tagEyeTracker2BlinkEventType
    {	ET2_BET_START	= 0,
	ET2_BET_STOP	= 1,
	ET2_BET_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2BlinkEventType;

typedef /* [uuid] */  DECLSPEC_UUID("803e40a6-d269-4af6-b20f-2575c6ae9337") 
enum tagEyeTracker2SaccadeEventType
    {	ET2_SET_START	= 0,
	ET2_SET_STOP	= 1,
	ET2_SET_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2SaccadeEventType;

typedef /* [uuid] */  DECLSPEC_UUID("b073ae96-95de-4dc2-8528-cfc9b54f5c16") 
enum tagEyeTracker2FixationEventType
    {	ET2_FET_START	= 0,
	ET2_FET_STOP	= 1,
	ET2_FET_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2FixationEventType;

typedef /* [uuid] */  DECLSPEC_UUID("f2ce25f7-c027-4238-90e6-b6213fe92980") 
enum tagEyeTracker2AOIEventType
    {	ET2_AET_ENTER	= 0,
	ET2_AET_EXIT	= 1,
	ET2_AET_FORCE_DWORD	= 0x7fffffff
    } 	EyeTracker2AOIEventType;




EXTERN_C const IID LIBID_EyeTracker2Lib;
#endif /* __EyeTracker2Lib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


