

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Wed Jun 04 15:08:29 2008
 */
/* Compiler settings for EyeTracker.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __EyeTracker_h__
#define __EyeTracker_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ICalibrationDisplay_FWD_DEFINED__
#define __ICalibrationDisplay_FWD_DEFINED__
typedef interface ICalibrationDisplay ICalibrationDisplay;
#endif 	/* __ICalibrationDisplay_FWD_DEFINED__ */


#ifndef __IEyeTracker_FWD_DEFINED__
#define __IEyeTracker_FWD_DEFINED__
typedef interface IEyeTracker IEyeTracker;
#endif 	/* __IEyeTracker_FWD_DEFINED__ */


#ifndef __ICalibrationDisplay_FWD_DEFINED__
#define __ICalibrationDisplay_FWD_DEFINED__
typedef interface ICalibrationDisplay ICalibrationDisplay;
#endif 	/* __ICalibrationDisplay_FWD_DEFINED__ */


#ifndef __IEyeTracker_FWD_DEFINED__
#define __IEyeTracker_FWD_DEFINED__
typedef interface IEyeTracker IEyeTracker;
#endif 	/* __IEyeTracker_FWD_DEFINED__ */


/* header files for imported files */
#include "unknwn.h"
#include "oaidl.h"
#include "InterfaceUtilities.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_EyeTracker_0000 */
/* [local] */ 

typedef /* [uuid] */  DECLSPEC_UUID("E2E51BEA-5F33-47F0-BDE3-63F7AEB2D784") struct PositionData
    {
    unsigned __int64 time;
    double x;
    double y;
    } 	PositionData;

typedef /* [uuid] */  DECLSPEC_UUID("358980FB-1A80-4196-9924-72E60A97C41A") struct AOIEventData
    {
    unsigned __int64 time;
    long area;
    } 	AOIEventData;

typedef /* [uuid] */  DECLSPEC_UUID("A6B47CBB-530F-42E5-8D54-E09C50F806AC") struct TriggerEventData
    {
    unsigned int time;
    long code;
    } 	TriggerEventData;

typedef /* [uuid] */  DECLSPEC_UUID("C64C17EF-0441-4977-A636-F5421843ABF6") 
enum tagSaccadeEventType
    {	SET_START	= 0,
	SET_STOP	= 1,
	SET_FORCE_DWORD	= 0x7fffffff
    } 	SaccadeEventType;

typedef /* [uuid] */  DECLSPEC_UUID("D35956EC-079F-4477-9FCB-189FE5E1BCDF") 
enum tagFixationEventType
    {	FET_START	= 0,
	FET_STOP	= 1,
	FET_FORCE_DWORD	= 0x7fffffff
    } 	FixationEventType;

typedef /* [uuid] */  DECLSPEC_UUID("73AE779F-D3C3-4F37-A1CF-EB2585B15DDB") 
enum tagEyeType
    {	ET_DEFAULT	= 0,
	ET_RIGHT	= 1,
	ET_LEFT	= 2,
	ET_FORCE_DWORD	= 0x7fffffff
    } 	EyeType;

typedef /* [uuid] */  DECLSPEC_UUID("46301A1B-58E1-44B5-A59A-FF263B080C24") struct PupilData
    {
    unsigned __int64 time;
    double xDiameter;
    double yDiameter;
    double area;
    } 	PupilData;

typedef /* [uuid] */  DECLSPEC_UUID("55F23A69-7B95-49B0-B2E4-8089B39EE7AF") 
enum tagBlinkEventType
    {	BET_START	= 0,
	BET_STOP	= 1,
	BET_FORCE_DWORD	= 0x7fffffff
    } 	BlinkEventType;

typedef /* [uuid] */  DECLSPEC_UUID("2B3C32FC-C913-4296-8845-4447A87D9D90") 
enum tagCalibrationType
    {	Calibrate	= 0,
	DRIFT_CORRECT	= 1,
	RESERVED	= 2,
	RESERVED1	= 3,
	RESERVED2	= 4,
	RESERVED3	= 5,
	RESERVED4	= 6,
	RESERVED5	= 7,
	RESERVED6	= 8,
	RESERVED7	= 9,
	CALIBRATION_TYPE_FORCE_DWORD	= 0x7fffffff
    } 	CalibrationType;

typedef /* [uuid] */  DECLSPEC_UUID("8C109B91-532C-4F14-B05B-C24E719F2530") struct SaccadeEventData
    {
    unsigned __int64 time;
    double x;
    double y;
    SaccadeEventType type;
    } 	SaccadeEventData;

typedef /* [uuid] */  DECLSPEC_UUID("82797C1F-78BD-4040-ABC6-5943FC3DE348") struct FixationEventData
    {
    unsigned __int64 time;
    double x;
    double y;
    FixationEventType type;
    } 	FixationEventData;

typedef /* [uuid] */  DECLSPEC_UUID("DFD1C113-4D8B-4782-B4D0-7127A69EFF87") struct BlinkEventData
    {
    unsigned __int64 time;
    double x;
    double y;
    BlinkEventType type;
    } 	BlinkEventData;



extern RPC_IF_HANDLE __MIDL_itf_EyeTracker_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_EyeTracker_0000_v0_0_s_ifspec;

#ifndef __ICalibrationDisplay_INTERFACE_DEFINED__
#define __ICalibrationDisplay_INTERFACE_DEFINED__

/* interface ICalibrationDisplay */
/* [object][oleautomation][dual][uuid] */ 


EXTERN_C const IID IID_ICalibrationDisplay;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7ED9FF4F-21B2-40EC-82B4-FF097181CF68")
    ICalibrationDisplay : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT __stdcall GetDisplayMode( 
            /* [out] */ ImageParameters *mode) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetPosition( 
            /* [in] */ short x,
            /* [in] */ short y) = 0;
        
        virtual /* [id] */ HRESULT __stdcall Present( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetLastError( 
            /* [out] */ BSTR *message) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetResponse( 
            /* [in] */ unsigned long timeOut,
            /* [out] */ unsigned short *button) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetKeypress( 
            /* [in] */ unsigned long timeOut,
            /* [out] */ unsigned short *key) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetImage( 
            /* [in] */ IImageData *image,
            /* [in] */ Palette *Palette) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetBackgroundColor( 
            /* [in] */ short red,
            /* [in] */ short green,
            /* [in] */ short blue) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICalibrationDisplayVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICalibrationDisplay * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICalibrationDisplay * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICalibrationDisplay * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICalibrationDisplay * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICalibrationDisplay * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICalibrationDisplay * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICalibrationDisplay * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( __stdcall *GetDisplayMode )( 
            ICalibrationDisplay * This,
            /* [out] */ ImageParameters *mode);
        
        /* [id] */ HRESULT ( __stdcall *SetPosition )( 
            ICalibrationDisplay * This,
            /* [in] */ short x,
            /* [in] */ short y);
        
        /* [id] */ HRESULT ( __stdcall *Present )( 
            ICalibrationDisplay * This);
        
        /* [id] */ HRESULT ( __stdcall *GetLastError )( 
            ICalibrationDisplay * This,
            /* [out] */ BSTR *message);
        
        /* [id] */ HRESULT ( __stdcall *GetResponse )( 
            ICalibrationDisplay * This,
            /* [in] */ unsigned long timeOut,
            /* [out] */ unsigned short *button);
        
        /* [id] */ HRESULT ( __stdcall *GetKeypress )( 
            ICalibrationDisplay * This,
            /* [in] */ unsigned long timeOut,
            /* [out] */ unsigned short *key);
        
        /* [id] */ HRESULT ( __stdcall *SetImage )( 
            ICalibrationDisplay * This,
            /* [in] */ IImageData *image,
            /* [in] */ Palette *Palette);
        
        /* [id] */ HRESULT ( __stdcall *SetBackgroundColor )( 
            ICalibrationDisplay * This,
            /* [in] */ short red,
            /* [in] */ short green,
            /* [in] */ short blue);
        
        END_INTERFACE
    } ICalibrationDisplayVtbl;

    interface ICalibrationDisplay
    {
        CONST_VTBL struct ICalibrationDisplayVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICalibrationDisplay_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICalibrationDisplay_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICalibrationDisplay_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICalibrationDisplay_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICalibrationDisplay_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICalibrationDisplay_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICalibrationDisplay_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICalibrationDisplay_GetDisplayMode(This,mode)	\
    (This)->lpVtbl -> GetDisplayMode(This,mode)

#define ICalibrationDisplay_SetPosition(This,x,y)	\
    (This)->lpVtbl -> SetPosition(This,x,y)

#define ICalibrationDisplay_Present(This)	\
    (This)->lpVtbl -> Present(This)

#define ICalibrationDisplay_GetLastError(This,message)	\
    (This)->lpVtbl -> GetLastError(This,message)

#define ICalibrationDisplay_GetResponse(This,timeOut,button)	\
    (This)->lpVtbl -> GetResponse(This,timeOut,button)

#define ICalibrationDisplay_GetKeypress(This,timeOut,key)	\
    (This)->lpVtbl -> GetKeypress(This,timeOut,key)

#define ICalibrationDisplay_SetImage(This,image,Palette)	\
    (This)->lpVtbl -> SetImage(This,image,Palette)

#define ICalibrationDisplay_SetBackgroundColor(This,red,green,blue)	\
    (This)->lpVtbl -> SetBackgroundColor(This,red,green,blue)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT __stdcall ICalibrationDisplay_GetDisplayMode_Proxy( 
    ICalibrationDisplay * This,
    /* [out] */ ImageParameters *mode);


void __RPC_STUB ICalibrationDisplay_GetDisplayMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall ICalibrationDisplay_SetPosition_Proxy( 
    ICalibrationDisplay * This,
    /* [in] */ short x,
    /* [in] */ short y);


void __RPC_STUB ICalibrationDisplay_SetPosition_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall ICalibrationDisplay_Present_Proxy( 
    ICalibrationDisplay * This);


void __RPC_STUB ICalibrationDisplay_Present_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall ICalibrationDisplay_GetLastError_Proxy( 
    ICalibrationDisplay * This,
    /* [out] */ BSTR *message);


void __RPC_STUB ICalibrationDisplay_GetLastError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall ICalibrationDisplay_GetResponse_Proxy( 
    ICalibrationDisplay * This,
    /* [in] */ unsigned long timeOut,
    /* [out] */ unsigned short *button);


void __RPC_STUB ICalibrationDisplay_GetResponse_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall ICalibrationDisplay_GetKeypress_Proxy( 
    ICalibrationDisplay * This,
    /* [in] */ unsigned long timeOut,
    /* [out] */ unsigned short *key);


void __RPC_STUB ICalibrationDisplay_GetKeypress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall ICalibrationDisplay_SetImage_Proxy( 
    ICalibrationDisplay * This,
    /* [in] */ IImageData *image,
    /* [in] */ Palette *Palette);


void __RPC_STUB ICalibrationDisplay_SetImage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall ICalibrationDisplay_SetBackgroundColor_Proxy( 
    ICalibrationDisplay * This,
    /* [in] */ short red,
    /* [in] */ short green,
    /* [in] */ short blue);


void __RPC_STUB ICalibrationDisplay_SetBackgroundColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICalibrationDisplay_INTERFACE_DEFINED__ */


#ifndef __IEyeTracker_INTERFACE_DEFINED__
#define __IEyeTracker_INTERFACE_DEFINED__

/* interface IEyeTracker */
/* [object][oleautomation][dual][version][uuid] */ 


EXTERN_C const IID IID_IEyeTracker;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("24D9A6F3-1D31-44B5-9DEB-C4D846D3DB71")
    IEyeTracker : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT __stdcall GetLastError( 
            /* [out] */ LPWSTR *message) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetCaps( 
            /* [out] */ long *caps) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetStatus( 
            /* [out] */ long *status) = 0;
        
        virtual /* [id] */ HRESULT __stdcall IsMultithreaded( 
            /* [out] */ VARIANT_BOOL *multiThreaded) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SendTrigger( 
            /* [in] */ long code) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SendString( 
            /* [in] */ LPWSTR message) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetTriggerEventCount( 
            /* [out] */ long *count) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetTriggerEvents( 
            /* [out][in] */ long *count,
            /* [out] */ TriggerEventData *position) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartTracking( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopTracking( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetRecording( 
            /* [in] */ VARIANT_BOOL recordingOn) = 0;
        
        virtual /* [id] */ HRESULT __stdcall Calibrate( 
            /* [in] */ CalibrationType calibration_type,
            /* [in] */ ICalibrationDisplay *interface_ptr,
            /* [in] */ double parameter1,
            /* [in] */ double parameter2,
            /* [in] */ double parameter3) = 0;
        
        virtual /* [id] */ HRESULT __stdcall Execute( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartPositionData( 
            /* [in] */ VARIANT_BOOL bufferData,
            /* [in] */ EyeType eye) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopPositionData( 
            /* [in] */ EyeType eye) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetPositionDataCount( 
            /* [out] */ long *count,
            /* [in] */ EyeType eye) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetPositionData( 
            /* [out][in] */ long *count,
            /* [out] */ PositionData *data,
            /* [in] */ EyeType eye) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartSaccadeEvents( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopSaccadeEvents( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetSaccadeEventCount( 
            /* [out] */ long *count) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetSaccadeEvents( 
            /* [out][in] */ long *count,
            /* [out] */ SaccadeEventData *data) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartFixationEvents( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopFixationEvents( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetFixationEventCount( 
            /* [out] */ long *count) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetFixationEvents( 
            /* [out][in] */ long *count,
            /* [out] */ FixationEventData *data) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartAOIEvents( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopAOIEvents( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetAOIEventCount( 
            /* [out] */ long *count) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetAOIEvents( 
            /* [out][in] */ long *count,
            /* [out] */ AOIEventData *position) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartPupilData( 
            /* [in] */ VARIANT_BOOL bufferData,
            /* [in] */ EyeType eye) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopPupilData( 
            /* [in] */ EyeType eye) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetPupilDataCount( 
            /* [out] */ long *count) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetPupilData( 
            /* [out][in] */ long *count,
            /* [out] */ PupilData *data,
            /* [in] */ EyeType eye) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SetAOISet( 
            /* [in] */ long id) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetTime( 
            /* [out] */ unsigned __int64 *time) = 0;
        
        virtual /* [id] */ HRESULT __stdcall SendCommand( 
            /* [in] */ LPWSTR message,
            /* [out] */ long *return_value) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StartBlinkEvents( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall StopBlinkEvents( void) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetBlinkEventCount( 
            /* [out] */ long *count) = 0;
        
        virtual /* [id] */ HRESULT __stdcall GetBlinkEvents( 
            /* [out][in] */ long *count,
            /* [out] */ BlinkEventData *data) = 0;
        
        virtual /* [id] */ HRESULT __stdcall IsRecording( 
            /* [out] */ VARIANT_BOOL *recording) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEyeTrackerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEyeTracker * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEyeTracker * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEyeTracker * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEyeTracker * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEyeTracker * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEyeTracker * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEyeTracker * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( __stdcall *GetLastError )( 
            IEyeTracker * This,
            /* [out] */ LPWSTR *message);
        
        /* [id] */ HRESULT ( __stdcall *GetCaps )( 
            IEyeTracker * This,
            /* [out] */ long *caps);
        
        /* [id] */ HRESULT ( __stdcall *GetStatus )( 
            IEyeTracker * This,
            /* [out] */ long *status);
        
        /* [id] */ HRESULT ( __stdcall *IsMultithreaded )( 
            IEyeTracker * This,
            /* [out] */ VARIANT_BOOL *multiThreaded);
        
        /* [id] */ HRESULT ( __stdcall *SendTrigger )( 
            IEyeTracker * This,
            /* [in] */ long code);
        
        /* [id] */ HRESULT ( __stdcall *SendString )( 
            IEyeTracker * This,
            /* [in] */ LPWSTR message);
        
        /* [id] */ HRESULT ( __stdcall *GetTriggerEventCount )( 
            IEyeTracker * This,
            /* [out] */ long *count);
        
        /* [id] */ HRESULT ( __stdcall *GetTriggerEvents )( 
            IEyeTracker * This,
            /* [out][in] */ long *count,
            /* [out] */ TriggerEventData *position);
        
        /* [id] */ HRESULT ( __stdcall *StartTracking )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *StopTracking )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *SetRecording )( 
            IEyeTracker * This,
            /* [in] */ VARIANT_BOOL recordingOn);
        
        /* [id] */ HRESULT ( __stdcall *Calibrate )( 
            IEyeTracker * This,
            /* [in] */ CalibrationType calibration_type,
            /* [in] */ ICalibrationDisplay *interface_ptr,
            /* [in] */ double parameter1,
            /* [in] */ double parameter2,
            /* [in] */ double parameter3);
        
        /* [id] */ HRESULT ( __stdcall *Execute )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *StartPositionData )( 
            IEyeTracker * This,
            /* [in] */ VARIANT_BOOL bufferData,
            /* [in] */ EyeType eye);
        
        /* [id] */ HRESULT ( __stdcall *StopPositionData )( 
            IEyeTracker * This,
            /* [in] */ EyeType eye);
        
        /* [id] */ HRESULT ( __stdcall *GetPositionDataCount )( 
            IEyeTracker * This,
            /* [out] */ long *count,
            /* [in] */ EyeType eye);
        
        /* [id] */ HRESULT ( __stdcall *GetPositionData )( 
            IEyeTracker * This,
            /* [out][in] */ long *count,
            /* [out] */ PositionData *data,
            /* [in] */ EyeType eye);
        
        /* [id] */ HRESULT ( __stdcall *StartSaccadeEvents )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *StopSaccadeEvents )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *GetSaccadeEventCount )( 
            IEyeTracker * This,
            /* [out] */ long *count);
        
        /* [id] */ HRESULT ( __stdcall *GetSaccadeEvents )( 
            IEyeTracker * This,
            /* [out][in] */ long *count,
            /* [out] */ SaccadeEventData *data);
        
        /* [id] */ HRESULT ( __stdcall *StartFixationEvents )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *StopFixationEvents )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *GetFixationEventCount )( 
            IEyeTracker * This,
            /* [out] */ long *count);
        
        /* [id] */ HRESULT ( __stdcall *GetFixationEvents )( 
            IEyeTracker * This,
            /* [out][in] */ long *count,
            /* [out] */ FixationEventData *data);
        
        /* [id] */ HRESULT ( __stdcall *StartAOIEvents )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *StopAOIEvents )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *GetAOIEventCount )( 
            IEyeTracker * This,
            /* [out] */ long *count);
        
        /* [id] */ HRESULT ( __stdcall *GetAOIEvents )( 
            IEyeTracker * This,
            /* [out][in] */ long *count,
            /* [out] */ AOIEventData *position);
        
        /* [id] */ HRESULT ( __stdcall *StartPupilData )( 
            IEyeTracker * This,
            /* [in] */ VARIANT_BOOL bufferData,
            /* [in] */ EyeType eye);
        
        /* [id] */ HRESULT ( __stdcall *StopPupilData )( 
            IEyeTracker * This,
            /* [in] */ EyeType eye);
        
        /* [id] */ HRESULT ( __stdcall *GetPupilDataCount )( 
            IEyeTracker * This,
            /* [out] */ long *count);
        
        /* [id] */ HRESULT ( __stdcall *GetPupilData )( 
            IEyeTracker * This,
            /* [out][in] */ long *count,
            /* [out] */ PupilData *data,
            /* [in] */ EyeType eye);
        
        /* [id] */ HRESULT ( __stdcall *SetAOISet )( 
            IEyeTracker * This,
            /* [in] */ long id);
        
        /* [id] */ HRESULT ( __stdcall *GetTime )( 
            IEyeTracker * This,
            /* [out] */ unsigned __int64 *time);
        
        /* [id] */ HRESULT ( __stdcall *SendCommand )( 
            IEyeTracker * This,
            /* [in] */ LPWSTR message,
            /* [out] */ long *return_value);
        
        /* [id] */ HRESULT ( __stdcall *StartBlinkEvents )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *StopBlinkEvents )( 
            IEyeTracker * This);
        
        /* [id] */ HRESULT ( __stdcall *GetBlinkEventCount )( 
            IEyeTracker * This,
            /* [out] */ long *count);
        
        /* [id] */ HRESULT ( __stdcall *GetBlinkEvents )( 
            IEyeTracker * This,
            /* [out][in] */ long *count,
            /* [out] */ BlinkEventData *data);
        
        /* [id] */ HRESULT ( __stdcall *IsRecording )( 
            IEyeTracker * This,
            /* [out] */ VARIANT_BOOL *recording);
        
        END_INTERFACE
    } IEyeTrackerVtbl;

    interface IEyeTracker
    {
        CONST_VTBL struct IEyeTrackerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEyeTracker_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEyeTracker_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEyeTracker_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEyeTracker_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEyeTracker_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEyeTracker_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEyeTracker_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEyeTracker_GetLastError(This,message)	\
    (This)->lpVtbl -> GetLastError(This,message)

#define IEyeTracker_GetCaps(This,caps)	\
    (This)->lpVtbl -> GetCaps(This,caps)

#define IEyeTracker_GetStatus(This,status)	\
    (This)->lpVtbl -> GetStatus(This,status)

#define IEyeTracker_IsMultithreaded(This,multiThreaded)	\
    (This)->lpVtbl -> IsMultithreaded(This,multiThreaded)

#define IEyeTracker_SendTrigger(This,code)	\
    (This)->lpVtbl -> SendTrigger(This,code)

#define IEyeTracker_SendString(This,message)	\
    (This)->lpVtbl -> SendString(This,message)

#define IEyeTracker_GetTriggerEventCount(This,count)	\
    (This)->lpVtbl -> GetTriggerEventCount(This,count)

#define IEyeTracker_GetTriggerEvents(This,count,position)	\
    (This)->lpVtbl -> GetTriggerEvents(This,count,position)

#define IEyeTracker_StartTracking(This)	\
    (This)->lpVtbl -> StartTracking(This)

#define IEyeTracker_StopTracking(This)	\
    (This)->lpVtbl -> StopTracking(This)

#define IEyeTracker_SetRecording(This,recordingOn)	\
    (This)->lpVtbl -> SetRecording(This,recordingOn)

#define IEyeTracker_Calibrate(This,calibration_type,interface_ptr,parameter1,parameter2,parameter3)	\
    (This)->lpVtbl -> Calibrate(This,calibration_type,interface_ptr,parameter1,parameter2,parameter3)

#define IEyeTracker_Execute(This)	\
    (This)->lpVtbl -> Execute(This)

#define IEyeTracker_StartPositionData(This,bufferData,eye)	\
    (This)->lpVtbl -> StartPositionData(This,bufferData,eye)

#define IEyeTracker_StopPositionData(This,eye)	\
    (This)->lpVtbl -> StopPositionData(This,eye)

#define IEyeTracker_GetPositionDataCount(This,count,eye)	\
    (This)->lpVtbl -> GetPositionDataCount(This,count,eye)

#define IEyeTracker_GetPositionData(This,count,data,eye)	\
    (This)->lpVtbl -> GetPositionData(This,count,data,eye)

#define IEyeTracker_StartSaccadeEvents(This)	\
    (This)->lpVtbl -> StartSaccadeEvents(This)

#define IEyeTracker_StopSaccadeEvents(This)	\
    (This)->lpVtbl -> StopSaccadeEvents(This)

#define IEyeTracker_GetSaccadeEventCount(This,count)	\
    (This)->lpVtbl -> GetSaccadeEventCount(This,count)

#define IEyeTracker_GetSaccadeEvents(This,count,data)	\
    (This)->lpVtbl -> GetSaccadeEvents(This,count,data)

#define IEyeTracker_StartFixationEvents(This)	\
    (This)->lpVtbl -> StartFixationEvents(This)

#define IEyeTracker_StopFixationEvents(This)	\
    (This)->lpVtbl -> StopFixationEvents(This)

#define IEyeTracker_GetFixationEventCount(This,count)	\
    (This)->lpVtbl -> GetFixationEventCount(This,count)

#define IEyeTracker_GetFixationEvents(This,count,data)	\
    (This)->lpVtbl -> GetFixationEvents(This,count,data)

#define IEyeTracker_StartAOIEvents(This)	\
    (This)->lpVtbl -> StartAOIEvents(This)

#define IEyeTracker_StopAOIEvents(This)	\
    (This)->lpVtbl -> StopAOIEvents(This)

#define IEyeTracker_GetAOIEventCount(This,count)	\
    (This)->lpVtbl -> GetAOIEventCount(This,count)

#define IEyeTracker_GetAOIEvents(This,count,position)	\
    (This)->lpVtbl -> GetAOIEvents(This,count,position)

#define IEyeTracker_StartPupilData(This,bufferData,eye)	\
    (This)->lpVtbl -> StartPupilData(This,bufferData,eye)

#define IEyeTracker_StopPupilData(This,eye)	\
    (This)->lpVtbl -> StopPupilData(This,eye)

#define IEyeTracker_GetPupilDataCount(This,count)	\
    (This)->lpVtbl -> GetPupilDataCount(This,count)

#define IEyeTracker_GetPupilData(This,count,data,eye)	\
    (This)->lpVtbl -> GetPupilData(This,count,data,eye)

#define IEyeTracker_SetAOISet(This,id)	\
    (This)->lpVtbl -> SetAOISet(This,id)

#define IEyeTracker_GetTime(This,time)	\
    (This)->lpVtbl -> GetTime(This,time)

#define IEyeTracker_SendCommand(This,message,return_value)	\
    (This)->lpVtbl -> SendCommand(This,message,return_value)

#define IEyeTracker_StartBlinkEvents(This)	\
    (This)->lpVtbl -> StartBlinkEvents(This)

#define IEyeTracker_StopBlinkEvents(This)	\
    (This)->lpVtbl -> StopBlinkEvents(This)

#define IEyeTracker_GetBlinkEventCount(This,count)	\
    (This)->lpVtbl -> GetBlinkEventCount(This,count)

#define IEyeTracker_GetBlinkEvents(This,count,data)	\
    (This)->lpVtbl -> GetBlinkEvents(This,count,data)

#define IEyeTracker_IsRecording(This,recording)	\
    (This)->lpVtbl -> IsRecording(This,recording)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT __stdcall IEyeTracker_GetLastError_Proxy( 
    IEyeTracker * This,
    /* [out] */ LPWSTR *message);


void __RPC_STUB IEyeTracker_GetLastError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetCaps_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *caps);


void __RPC_STUB IEyeTracker_GetCaps_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetStatus_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *status);


void __RPC_STUB IEyeTracker_GetStatus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_IsMultithreaded_Proxy( 
    IEyeTracker * This,
    /* [out] */ VARIANT_BOOL *multiThreaded);


void __RPC_STUB IEyeTracker_IsMultithreaded_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_SendTrigger_Proxy( 
    IEyeTracker * This,
    /* [in] */ long code);


void __RPC_STUB IEyeTracker_SendTrigger_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_SendString_Proxy( 
    IEyeTracker * This,
    /* [in] */ LPWSTR message);


void __RPC_STUB IEyeTracker_SendString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetTriggerEventCount_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *count);


void __RPC_STUB IEyeTracker_GetTriggerEventCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetTriggerEvents_Proxy( 
    IEyeTracker * This,
    /* [out][in] */ long *count,
    /* [out] */ TriggerEventData *position);


void __RPC_STUB IEyeTracker_GetTriggerEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StartTracking_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StartTracking_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StopTracking_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StopTracking_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_SetRecording_Proxy( 
    IEyeTracker * This,
    /* [in] */ VARIANT_BOOL recordingOn);


void __RPC_STUB IEyeTracker_SetRecording_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_Calibrate_Proxy( 
    IEyeTracker * This,
    /* [in] */ CalibrationType calibration_type,
    /* [in] */ ICalibrationDisplay *interface_ptr,
    /* [in] */ double parameter1,
    /* [in] */ double parameter2,
    /* [in] */ double parameter3);


void __RPC_STUB IEyeTracker_Calibrate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_Execute_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_Execute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StartPositionData_Proxy( 
    IEyeTracker * This,
    /* [in] */ VARIANT_BOOL bufferData,
    /* [in] */ EyeType eye);


void __RPC_STUB IEyeTracker_StartPositionData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StopPositionData_Proxy( 
    IEyeTracker * This,
    /* [in] */ EyeType eye);


void __RPC_STUB IEyeTracker_StopPositionData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetPositionDataCount_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *count,
    /* [in] */ EyeType eye);


void __RPC_STUB IEyeTracker_GetPositionDataCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetPositionData_Proxy( 
    IEyeTracker * This,
    /* [out][in] */ long *count,
    /* [out] */ PositionData *data,
    /* [in] */ EyeType eye);


void __RPC_STUB IEyeTracker_GetPositionData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StartSaccadeEvents_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StartSaccadeEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StopSaccadeEvents_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StopSaccadeEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetSaccadeEventCount_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *count);


void __RPC_STUB IEyeTracker_GetSaccadeEventCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetSaccadeEvents_Proxy( 
    IEyeTracker * This,
    /* [out][in] */ long *count,
    /* [out] */ SaccadeEventData *data);


void __RPC_STUB IEyeTracker_GetSaccadeEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StartFixationEvents_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StartFixationEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StopFixationEvents_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StopFixationEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetFixationEventCount_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *count);


void __RPC_STUB IEyeTracker_GetFixationEventCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetFixationEvents_Proxy( 
    IEyeTracker * This,
    /* [out][in] */ long *count,
    /* [out] */ FixationEventData *data);


void __RPC_STUB IEyeTracker_GetFixationEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StartAOIEvents_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StartAOIEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StopAOIEvents_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StopAOIEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetAOIEventCount_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *count);


void __RPC_STUB IEyeTracker_GetAOIEventCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetAOIEvents_Proxy( 
    IEyeTracker * This,
    /* [out][in] */ long *count,
    /* [out] */ AOIEventData *position);


void __RPC_STUB IEyeTracker_GetAOIEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StartPupilData_Proxy( 
    IEyeTracker * This,
    /* [in] */ VARIANT_BOOL bufferData,
    /* [in] */ EyeType eye);


void __RPC_STUB IEyeTracker_StartPupilData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StopPupilData_Proxy( 
    IEyeTracker * This,
    /* [in] */ EyeType eye);


void __RPC_STUB IEyeTracker_StopPupilData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetPupilDataCount_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *count);


void __RPC_STUB IEyeTracker_GetPupilDataCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetPupilData_Proxy( 
    IEyeTracker * This,
    /* [out][in] */ long *count,
    /* [out] */ PupilData *data,
    /* [in] */ EyeType eye);


void __RPC_STUB IEyeTracker_GetPupilData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_SetAOISet_Proxy( 
    IEyeTracker * This,
    /* [in] */ long id);


void __RPC_STUB IEyeTracker_SetAOISet_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetTime_Proxy( 
    IEyeTracker * This,
    /* [out] */ unsigned __int64 *time);


void __RPC_STUB IEyeTracker_GetTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_SendCommand_Proxy( 
    IEyeTracker * This,
    /* [in] */ LPWSTR message,
    /* [out] */ long *return_value);


void __RPC_STUB IEyeTracker_SendCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StartBlinkEvents_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StartBlinkEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_StopBlinkEvents_Proxy( 
    IEyeTracker * This);


void __RPC_STUB IEyeTracker_StopBlinkEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetBlinkEventCount_Proxy( 
    IEyeTracker * This,
    /* [out] */ long *count);


void __RPC_STUB IEyeTracker_GetBlinkEventCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_GetBlinkEvents_Proxy( 
    IEyeTracker * This,
    /* [out][in] */ long *count,
    /* [out] */ BlinkEventData *data);


void __RPC_STUB IEyeTracker_GetBlinkEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT __stdcall IEyeTracker_IsRecording_Proxy( 
    IEyeTracker * This,
    /* [out] */ VARIANT_BOOL *recording);


void __RPC_STUB IEyeTracker_IsRecording_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEyeTracker_INTERFACE_DEFINED__ */



#ifndef __EyeTrackerLib_LIBRARY_DEFINED__
#define __EyeTrackerLib_LIBRARY_DEFINED__

/* library EyeTrackerLib */
/* [uuid] */ 

typedef /* [uuid] */  DECLSPEC_UUID("62013B22-D033-4950-B4E4-748D966A29F4") 
enum tagCapabilityFlags
    {	SUPPORTS_POSITION	= 1,
	SUPPORTS_PUPIL	= 2,
	SUPPORTS_FIXATION	= 4,
	SUPPORTS_SACCADE	= 8,
	SUPPORTS_AOI	= 16,
	SUPPORTS_BLINK	= 32,
	SUPPORTS_MULTIPLE_EYES	= 64,
	CAPABILITY_FLAGS_FORCE_DWORD	= 0x7fffffff
    } 	CapabilityFlags;

typedef /* [uuid] */  DECLSPEC_UUID("F018ABE5-1397-4B7F-8BA0-6D6C8676DFD2") 
enum tagEyeTrackerStatus
    {	ET_STATUS_TRACKING_LEFT	= 1,
	ET_STATUS_TRACKING_RIGHT	= 2,
	ET_STATUS_TRACKING	= 4,
	ET_STATUS_RECORDING	= 8,
	ET_STATUS_STOPPED	= 16,
	ET_STATUS_INITIALIZING	= 64,
	ET_STATUS_ERROR	= 128,
	ET_STATUS_FORCE_DWORD	= 0x7fffffff
    } 	EyeTrackerStatus;




EXTERN_C const IID LIBID_EyeTrackerLib;
#endif /* __EyeTrackerLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


