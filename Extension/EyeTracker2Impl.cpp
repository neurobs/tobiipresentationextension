// EyeTracker2Impl.cpp : Implementation of CEyeTracker2Impl

#include "stdafx.h"
#include "EyeTracker2Impl.h"
#include <algorithm>
#include <iterator>

using namespace nbs;
using namespace nbs::util;

namespace
{

BSTR create_string(const std::string& s)
{
	return SysAllocString(utf8_to_utf16(s).c_str());
}

}

// CEyeTracker2Impl

//---------------------------------------------------------------------------

CEyeTracker2Impl::CEyeTracker2Impl()
{
	try
	{
		Eye_tracker_list trackers;
		auto trackers_info = trackers.get_info();
		if (trackers_info.empty())
		{
			last_error_ = "No Tobii eye trackers were detected.";
		}
		else
		{
			check_call(tobii_research_get_eyetracker(trackers_info[0].address_.c_str(), &tracker_), "Error getting eye tracker object.");
		}
	}
	catch (std::exception& e)
	{
		last_error_ = e.what();
	}
	catch (...)
	{
		last_error_ = "Unknown exception occurred while getting eye tracker from Tobii API.";
	}
}

//---------------------------------------------------------------------------

CEyeTracker2Impl::~CEyeTracker2Impl()
{
	calibration_.reset();
	if (tobii_subscriptions_ & TOBII_SUBSCRIBE_GAZE)
	{
		tobii_research_unsubscribe_from_gaze_data(tracker_, gaze_callback);
	}
}

//---------------------------------------------------------------------------

HRESULT CEyeTracker2Impl::protect(State state, BSTR* return_value, std::function<void()> fun)
{
	HRESULT rval = E_FAIL;
	// If tracker_ is null, there's already an error message in last_error_.
	if (tracker_) {	
		rval = S_OK;
		std::string error;
		if ((state == State::CALIBRATION) && (!calibration_))
		{
			error = "This method may only be called during a manual calibration procedure.";
		}
		else if ((state == State::STANDARD) && calibration_)
		{
			error = "This method may not be called during a manual calibration procedure.";
		}
		else if ((state == State::IDLE) && (calibration_ || (tobii_subscriptions_ != 0)))
		{
			error = "This method may not be called during a manual calibration procedure or while tracking.";
		}
		else
		{
			try {
				fun();
			}
			catch (std::exception& e) {
				error = e.what();
			}
			catch (...) {
				error = "Unknown exception";
			}
		}
		if (return_value)
		{
			*return_value = create_string(error);
		}
		else if (!error.empty())
		{
			last_error_ = error;
			rval = E_FAIL;
		}
	}
	return rval;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::GetCaps(long * caps)
{
	*caps = SUPPORTS_POSITION | SUPPORTS_PUPIL | SUPPORTS_MULTIPLE_EYES;
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::IsMultithreaded(VARIANT_BOOL * multiThreaded)
{
	*multiThreaded = true;
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SetDisplayMode(long width, long height)
{
	shared_.screen_size_.width_ = width;
	shared_.screen_size_.height_ = height;
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::Execute()
{
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::GetTime(unsigned __int64 * time)
{
	return protect(State::ANY, nullptr, [this, time]() {
		int64_t system_time_stamp;
		check_call(tobii_research_get_system_time_stamp(&system_time_stamp), "Error getting Tobii system timestamp." );
		*time = system_time_stamp;
	});
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::GetStatus(long * status)
{
	*status =
		(!tracker_) ? ET2_STATUS_ERROR :
		(tobii_subscriptions_ == 0) ? ET2_STATUS_STOPPED :
		ET2_STATUS_TRACKING | ET2_STATUS_TRACKING_LEFT | ET2_STATUS_TRACKING_RIGHT;
	if (shared_.recording_)
	{
		*status |= ET2_STATUS_RECORDING;
	}
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::GetLastError(BSTR * message)
{
	*message = create_string(last_error_);
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::GetEventCount(long * count)
{
	*count = static_cast<long>(shared_.events_.size());
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::GetEvents(long * count, EyeTracker2GeneralData * events)
{
	*count = shared_.events_.retrieve(events, *count);
	return S_OK;
}

//---------------------------------------------------------------------------

Shared_data::Bitset_type Shared_data::get_bit(EyeTracker2DataType type, EyeTracker2EyeType eye)
{
	return
		(eye == ET2_ET_RIGHT) ? (
			(type == ET2_DT_POSITION) ? PRES_SUBSCRIBE_POSITION_RIGHT :
			(type == ET2_DT_PUPIL)    ? PRES_SUBSCRIBE_PUPIL_RIGHT :
			0
		) :
		(eye == ET2_ET_LEFT) ? (
			(type == ET2_DT_POSITION) ? PRES_SUBSCRIBE_POSITION_LEFT :
			(type == ET2_DT_PUPIL)    ? PRES_SUBSCRIBE_PUPIL_LEFT :
			0
		) : 
		0;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::StartData(EyeTracker2DataType type, EyeTracker2EyeType eye, BSTR * returnValue)
{
	std::string return_value;
	auto bit = Shared_data::get_bit(type, eye);
	if (!bit)
	{
		return_value = "This extension only supports position and pupil data with eye types RIGHT and LEFT. Use eye_tracker::set_default_data_set with eye_tracker::LEFT or eye_tracker::RIGHT.";
	}
	else
	{
		shared_.pres_subscriptions_.fetch_or( bit );
	}
	*returnValue = create_string(return_value);
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::StopData(EyeTracker2DataType type, EyeTracker2EyeType eye, BSTR * returnValue)
{
	auto bit = Shared_data::get_bit(type, eye);
	if (bit)
	{
		shared_.pres_subscriptions_.fetch_and(~bit);
	}
	*returnValue = create_string("");
	return S_OK;
}

//---------------------------------------------------------------------------

void Shared_data::add_position_event(EyeTracker2EyeType eye, const TobiiResearchGazeData& data, const TobiiResearchEyeData& eye_data)
{
	if (eye_data.gaze_point.validity == TOBII_RESEARCH_VALIDITY_VALID)
	{ 
		EyeTracker2GeneralData e;
		e.time = data.system_time_stamp;
		e.type = ET2_DT_POSITION;
		e.eye = eye;
		auto p = convert_point(eye_data.gaze_point.position_on_display_area, screen_size_);
		e.xData = p.x_;
		e.yData = p.y_;
		events_.push(e);
	}
}

void Shared_data::add_pupil_event(EyeTracker2EyeType eye, const TobiiResearchGazeData& data, const TobiiResearchEyeData& eye_data)
{
	if (eye_data.pupil_data.validity == TOBII_RESEARCH_VALIDITY_VALID)
	{
		EyeTracker2GeneralData e;
		e.time = data.system_time_stamp;
		e.type = ET2_DT_PUPIL;
		e.eye = eye;
		e.xData = e.yData = eye_data.pupil_data.diameter;
		e.data1 = 3.14159 * e.xData * e.xData / 4.0;
		events_.push(e);
	}
}

void CEyeTracker2Impl::gaze_callback(TobiiResearchGazeData *gaze_data, void *user_data)
{
	Shared_data* shared = static_cast<Shared_data*>(user_data);
	auto subs = shared->pres_subscriptions_.load();
	if (subs)
	{
		if (subs & Shared_data::PRES_SUBSCRIBE_POSITION_LEFT)
		{
			shared->add_position_event(ET2_ET_LEFT, *gaze_data, gaze_data->left_eye);
		}
		if (subs & Shared_data::PRES_SUBSCRIBE_POSITION_RIGHT)
		{
			shared->add_position_event(ET2_ET_RIGHT, *gaze_data, gaze_data->right_eye);
		}
		if (subs & Shared_data::PRES_SUBSCRIBE_PUPIL_LEFT)
		{
			shared->add_pupil_event(ET2_ET_LEFT, *gaze_data, gaze_data->left_eye);
		}
		if (subs & Shared_data::PRES_SUBSCRIBE_PUPIL_RIGHT)
		{
			shared->add_pupil_event(ET2_ET_RIGHT, *gaze_data, gaze_data->right_eye);
		}
	}
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SetTracking(VARIANT_BOOL trackingOn, BSTR * returnValue)
{
	return protect(State::STANDARD, returnValue, [this, trackingOn]() {
		bool current = tobii_subscriptions_ & TOBII_SUBSCRIBE_GAZE;
		if (trackingOn && (!current))
		{
			check_call(tobii_research_subscribe_to_gaze_data(tracker_, gaze_callback, &shared_), "Error subscribing to gaze data.");
			tobii_subscriptions_ |= TOBII_SUBSCRIBE_GAZE;
		}
		else if ((!trackingOn) && current)
		{
			check_call(tobii_research_unsubscribe_from_gaze_data(tracker_, gaze_callback), "Error unsubscribing to gaze data.");
			tobii_subscriptions_ &= ~TOBII_SUBSCRIBE_GAZE;
		}
	});
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SetRecording(VARIANT_BOOL recordingOn, BSTR * returnValue)
{
	shared_.recording_.store(recordingOn != 0);
	*returnValue = create_string("");
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::Calibrate(EyeTracker2CalibrationType calibrationType, ICalibrationDisplay * interfacePtr, double delay_ms, double parameter2, double parameter3, BSTR * returnValue)
{
	return protect(State::IDLE, returnValue, [this, calibrationType, interfacePtr, delay_ms]() {
		std::vector<TobiiResearchNormalizedPoint2D> temp = { {0.0f, 0.0f}, {0.1f, 0.1f}, {0.1f, 0.9f}, {0.9f, 0.1f}, {0.9f, 0.9f} };
		std::vector<Presentation_point> points;
		std::transform(temp.begin(), temp.end(), std::back_inserter(points), [this](TobiiResearchNormalizedPoint2D p) { return convert_point(p, shared_.screen_size_); });

		Calibration_procedure procedure(tracker_, points, static_cast<unsigned int>(delay_ms), interfacePtr, shared_.screen_size_, false);
		auto error = procedure.finish();
		if (!error.empty()) throw std::runtime_error(error);
	});
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::StartCalibration(long pointCount, const long * points, EyeTracker2EyeType eye, LPCWSTR parameters, BSTR * returnValue)
{
	return protect(State::IDLE, returnValue, [this, pointCount, &points, eye, parameters]() {
		std::vector<Presentation_point> point_list;
		for (long i = 0; i < pointCount; ++i, points += 2)
		{
			point_list.push_back({ *points, *(points + 1) });
		}
		auto delay_ms = std::stoi(parameters);
		calibration_.reset(new Calibration_procedure(tracker_, point_list, delay_ms, nullptr, shared_.screen_size_, true));
	});
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::GetCalibrationPoint(long * pointIndex, long * x, long * y, BSTR * returnValue)
{
	return protect(State::CALIBRATION, returnValue, [this, pointIndex, x, y]() {
		auto error = calibration_->current_point(*pointIndex, *x, *y);
		if (!error.empty()) throw std::runtime_error(error);
	});
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::AcceptPoint(BSTR * returnValue)
{
	*returnValue = create_string( "This extension does not support eye_tracker::accept_point." );
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::StopCalibration(BSTR * returnValue)
{
	return protect(State::CALIBRATION, returnValue, [this, returnValue]() {
		auto error = calibration_->finish();
		calibration_.reset();
		if (!error.empty()) throw std::runtime_error(error);
	});
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SendTrigger(long code)
{
	last_error_ = "This extension does not support eye_tracker::send_trigger.";
	return E_NOTIMPL;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SendString(LPCWSTR message)
{
	last_error_ = "This extension does not support eye_tracker::send_string.";
	return E_NOTIMPL;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SendCommand(LPCWSTR message, long * returnValue)
{
	last_error_ = "This extension does not support eye_tracker::send_command.";
	return E_NOTIMPL;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SendMessage(LPCWSTR message, BSTR * response)
{
	last_error_ = "This extension does not support eye_tracker::send_message.";
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SetParameter(LPCWSTR name, LPCWSTR value, BSTR * returnValue)
{
	std::string return_value;
	auto parameter = utf16_to_utf8(name);
	if (parameter == "address")
	{
		if ((tobii_subscriptions_ != 0) || calibration_)
		{
			return_value = "The eye tracker cannot be reset when it is tracking.";
		}
		else
		{
			auto address = utf16_to_utf8(value);
			try
			{
				check_call(tobii_research_get_eyetracker(address.c_str(), &tracker_), ("Error getting eye tracker object for address '" + address + "'.").c_str());
			}
			catch (std::exception& e)
			{
				return_value = e.what();
			}
		}
	}
	else
	{
		return_value = "Unknown parameter '" + parameter + "'.";
	}
	*returnValue = create_string(return_value);
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::GetParameter(LPCWSTR name, BSTR * value)
{
	*value = create_string("");
	return S_OK;
}

//---------------------------------------------------------------------------

STDMETHODIMP CEyeTracker2Impl::SetAOISet(long id, BSTR * returnValue)
{
	last_error_ = "This extension does not support eye_tracker::set_aoi_set.";
	return E_NOTIMPL;
}

//---------------------------------------------------------------------------

Calibration_procedure::Calibration_procedure(TobiiResearchEyeTracker* tracker, const std::vector<Presentation_point>& points, unsigned int delay_ms, ICalibrationDisplay* display, Screen_size screen_size, bool asynchronous)
	:
	tracker_(tracker),
	points_(points),
	index_(0),
	abort_(false),
	finished_(false)
{
	if (points.empty())
	{
		throw std::runtime_error("The calibration point list was empty.");
	}
	check_call(tobii_research_screen_based_calibration_enter_calibration_mode(tracker_),
		"Error entering calibration mode.");
	if (asynchronous)
	{
		thread_ = std::thread(&Calibration_procedure::thread_function, this, delay_ms, display, screen_size);
	}
	else
	{
		thread_function(delay_ms, display, screen_size);
		tobii_research_screen_based_calibration_leave_calibration_mode(tracker_);
	}
}

//---------------------------------------------------------------------------

void Calibration_procedure::thread_function(unsigned int delay_ms, ICalibrationDisplay* display, Screen_size screen_size)
{
	try
	{
		// This loop does not allow index_ to go out of 
		// range so the main thread can continue to return
		// the final point location until everything is done.
		while (!abort_)
		{
			if (display)
			{
				show_point(display, points_[index_]);
			}
			Sleep(delay_ms);
			if (!abort_)
			{
				TobiiResearchNormalizedPoint2D point = convert_point(points_[index_], screen_size);
				if (tobii_research_screen_based_calibration_collect_data(tracker_, point.x, point.y) != TOBII_RESEARCH_STATUS_OK)
				{
					// Try again if it didn't go well the first time.
					// Not all eye tracker models will fail at this point, but instead fail on ComputeAndApply.
					check_call(tobii_research_screen_based_calibration_collect_data(tracker_, point.x, point.y), "Error collecting calibration data.");
				}
			}
			if (index_ < points_.size() - 1)
			{
				++index_;
			}
			else break;
		}
		if (abort_)
		{
			error_ = "The calibration procedure was cancelled before finishing.";
		}
		else
		{
			Tobii_calibration_result result;
			check_call(tobii_research_screen_based_calibration_compute_and_apply(tracker_, result.assignee()), "Error computing calibration.");
			if (result.data()->status != TOBII_RESEARCH_CALIBRATION_SUCCESS)
			{
				error_ = "Calibration failed.";
			}
		}
	}
	catch (std::exception& e)
	{
		error_ = e.what();
	}
	catch (...)
	{
		error_ = "Unknown exception during calibration procedure.";
	}
	finished_ = true;
}

//---------------------------------------------------------------------------

std::string Calibration_procedure::current_point(long& base_1_index, long& x, long& y)
{
	if (finished_)
	{
		base_1_index = 0;
		return error_;
	}
	base_1_index = index_ + 1;
	auto& p = points_[base_1_index - 1];
	x = p.x_;
	y = p.y_;
	return "";
}

//---------------------------------------------------------------------------

std::string Calibration_procedure::finish()
{
	if (thread_.joinable())
	{
		abort_ = true;
		thread_.join();
		auto status = tobii_research_screen_based_calibration_leave_calibration_mode(tracker_);
		if (status != TOBII_RESEARCH_STATUS_OK)
		{
			if (error_.empty())
			{
				error_ = "Error leaving calibration mode.";
			}
		}
	}
	return error_;
}

//---------------------------------------------------------------------------

Calibration_procedure::~Calibration_procedure()
{
	finish();
}

//---------------------------------------------------------------------------

void Calibration_procedure::show_point(ICalibrationDisplay* display, Presentation_point point)
{
	auto result = display->SetPosition(static_cast<short>(point.x_), static_cast<short>(point.y_));
	if (result == S_OK)
	{
		result = display->Present();
	}
	if (result != S_OK)
	{
		BSTR error = nullptr;
		result = display->GetLastError(&error);
		if (result == S_OK)
		{
			auto temp = utf16_to_utf8(error);
			SysFreeString(error);
			throw std::runtime_error("Error displaying calibration point: " + temp);
		}
		throw std::runtime_error("Unknown error displaying calibration point.");
	}
}

//---------------------------------------------------------------------------
