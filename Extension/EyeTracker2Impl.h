// EyeTracker2Impl.h : Declaration of the CEyeTracker2Impl

#pragma once
#include "resource.h"       // main symbols
#include "EyeTracker2.h"
#include <string>
#include "nbs/util/tobii_util.h"
#include "nbs/util/locked_deque.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif


// IEyeTracker2Impl
[
	object,
	uuid("87694F07-2260-4138-8046-4DA5577F6963"),
	dual,	helpstring("IEyeTracker2Impl Interface"),
	pointer_default(unique)
]
__interface IEyeTracker2Impl : IDispatch
{
};

/// \brief Data shared between the main thread and the Tobii API callback functions
struct Shared_data
{
	Shared_data() : events_(MAX_QUEUE_SIZE) {}

	typedef unsigned int Bitset_type;
	static const Bitset_type PRES_SUBSCRIBE_POSITION_LEFT =  1 << 0;
	static const Bitset_type PRES_SUBSCRIBE_POSITION_RIGHT = 1 << 1;
	static const Bitset_type PRES_SUBSCRIBE_PUPIL_LEFT =     1 << 2;
	static const Bitset_type PRES_SUBSCRIBE_PUPIL_RIGHT =    1 << 3;
	static Bitset_type get_bit(EyeTracker2DataType type, EyeTracker2EyeType eye);

	/// \brief Types of data Presentation is subscribed to
	std::atomic<Bitset_type> pres_subscriptions_ = 0;

	/// \brief Whether data recording is on
	std::atomic<bool> recording_ = false;

	/// \note Presentation sets the display mode as the first thing it does and
	/// never changes it, so this is treated as a constant
	nbs::util::Screen_size screen_size_;

	/// \brief Event data sent back from the Tobii callback function(s)
	nbs::util::Locked_deque<EyeTracker2GeneralData> events_;

	void add_position_event(EyeTracker2EyeType eye, const TobiiResearchGazeData& data, const TobiiResearchEyeData& eye_data);
	void add_pupil_event(EyeTracker2EyeType eye, const TobiiResearchGazeData& data, const TobiiResearchEyeData& eye_data);

	static const int MAX_QUEUE_SIZE = 1024;
};

/// \brief Handles the calibration procedure in a second thread
class Calibration_procedure
{
public:
	/// \param display May be nullptr if the procedure 
	/// is not displaying the stimulus
	///
	/// \param delay_ms Wait time in ms after switching
	/// to a new point before calibration data is
	/// collected.
	///
	/// \param asynchronous If true, this object manages
	/// a second thread. If false, the entire calibration 
	/// is done in the constructor synchronously.
	Calibration_procedure(TobiiResearchEyeTracker* tracker, const std::vector<nbs::util::Presentation_point>& points, unsigned int delay_ms, ICalibrationDisplay* display, nbs::util::Screen_size screen_size, bool asynchronous);
	~Calibration_procedure();

	/// \brief Gets the current calibration point for implementing GetCalibrationPoint
	///
	/// \param base_1_index base 1 index of current point, or 0 if calibration is finished
	///
	/// \return Error message if any errors have occurred during the procedure, or empty string otherwise
	std::string current_point(long& base_1_index, long& x, long& y);

	/// \brief Gets result of calibration procedure, or tries to abort it if still in progress
	///
	/// Call this after current_point returns 0 for base_1_index to get the result
	/// of the calibration and to exit calibration mode. If called before then, it will try
	/// to abort the procedure. This method will not return until the calibration is finished.
	///
	/// \return Error message, or empty string. 
	std::string finish();

	Calibration_procedure(const Calibration_procedure&) = delete;
	void operator=(const Calibration_procedure&) = delete;

private:
	TobiiResearchEyeTracker* const tracker_;
	const std::vector<nbs::util::Presentation_point> points_;
	std::thread thread_;
	/// Current point index (base 0) updated by the second thread. 
	/// This will never be >= points_.size, even when finished
	std::atomic<unsigned int> index_;
	/// Used to signal to the second thread to try to abort
	std::atomic<bool> abort_;
	/// Signals that the second thread is finished
	std::atomic<bool> finished_;
	/// Error message written by the 2nd thread. Do not read this
	/// member unless finished_ is true.
	std::string error_;

	void thread_function(unsigned int delay_ms, ICalibrationDisplay* display, nbs::util::Screen_size screen_size);

	/// \throw exception Error returned by ICalibrationDisplay
	static void show_point(ICalibrationDisplay* display, nbs::util::Presentation_point point);
};

// CEyeTracker2Impl

[
	coclass,
	default(IEyeTracker2Impl),
	implements(interfaces={IEyeTracker2},dispinterfaces={IEyeTracker2Impl}),
	threading(apartment),
	vi_progid("eyetracker2_template.EyeTracker2Impl"),
	progid("eyetracker2_template.EyeTracker2Impl.1"),
	version(1.0),
	uuid("CF3B9525-1230-4E7D-B152-C7561527D4AC"),
	helpstring("EyeTracker2Impl Class")
]
class ATL_NO_VTABLE CEyeTracker2Impl :
	public IEyeTracker2Impl,
	public IDispatchImpl<IEyeTracker2, &__uuidof(IEyeTracker2), &LIBID_EyeTracker2Lib>
{
private:
	TobiiResearchEyeTracker* tracker_ = nullptr;
	std::string last_error_;

	typedef unsigned int Bitset_type;
	static const Bitset_type TOBII_SUBSCRIBE_GAZE = 1;
	/// \brief Bitset of things currently subscribed to with the Tobii API
	Bitset_type tobii_subscriptions_ = 0;

	/// \brief Data shared with callbacks
	Shared_data shared_;

	/// \brief Calibration object if calibration is in progress, or null otherwise
	std::unique_ptr<Calibration_procedure> calibration_;

	/// \brief Callback for tobii_research_subscribe_to_gaze_data
	static void gaze_callback(TobiiResearchGazeData *gaze_data, void *user_data);

	enum class State {
		ANY,        ///< Any state
		STANDARD,   ///< Not calibrating
		IDLE,       ///< Not calibrating and not tracking
		CALIBRATION ///< Calibratin
	};
	/// \brief Handles state requirements, exception handling, error reporting for IEyeTracker2 methods
	///
	/// Methods that require tracker_ to be non-null, require exception handling, or require the
	/// object to be in a certain state can wrap their implementation in a call to this method.
	///
	/// \param return_value Call be null if the method does not return an error string
	HRESULT protect(State state_requirement, BSTR* return_value, std::function<void()> fun);

public:
	CEyeTracker2Impl();
	~CEyeTracker2Impl();

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

	// IEyeTracker2 Methods
	STDMETHOD(GetCaps)(long * caps);
	STDMETHOD(IsMultithreaded)(VARIANT_BOOL * multiThreaded);
	STDMETHOD(SetDisplayMode)(long width, long height);
	STDMETHOD(Execute)();
	STDMETHOD(GetTime)(unsigned __int64 * time);
	STDMETHOD(GetStatus)(long * status);
	STDMETHOD(GetLastError)(BSTR * message);
	STDMETHOD(GetEventCount)(long * count);
	STDMETHOD(GetEvents)(long * count, EyeTracker2GeneralData * events);
	STDMETHOD(StartData)(EyeTracker2DataType type, EyeTracker2EyeType eye, BSTR * returnValue);
	STDMETHOD(StopData)(EyeTracker2DataType type, EyeTracker2EyeType eye, BSTR * returnValue);
	STDMETHOD(SetTracking)(VARIANT_BOOL trackingOn, BSTR * returnValue);
	STDMETHOD(SetRecording)(VARIANT_BOOL recordingOn, BSTR * returnValue);
	STDMETHOD(Calibrate)(EyeTracker2CalibrationType calibrationType, ICalibrationDisplay * interfacePtr, double parameter1, double parameter2, double parameter3, BSTR * returnValue);
	STDMETHOD(StartCalibration)(long pointCount, const long * points, EyeTracker2EyeType eye, LPCWSTR parameters, BSTR * returnValue);
	STDMETHOD(GetCalibrationPoint)(long * pointIndex, long * x, long * y, BSTR * returnValue);
	STDMETHOD(AcceptPoint)(BSTR * returnValue);
	STDMETHOD(StopCalibration)(BSTR * returnValue);
	STDMETHOD(SendTrigger)(long code);
	STDMETHOD(SendString)(LPCWSTR message);
	STDMETHOD(SendCommand)(LPCWSTR message, long * returnValue);
	STDMETHOD(SendMessage)(LPCWSTR message, BSTR * response);
	STDMETHOD(SetParameter)(LPCWSTR name, LPCWSTR value, BSTR * returnValue);
	STDMETHOD(GetParameter)(LPCWSTR name, BSTR * value);
	STDMETHOD(SetAOISet)(long id, BSTR * returnValue);
};

