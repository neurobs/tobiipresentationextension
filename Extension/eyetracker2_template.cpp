// eyetracker2_template.cpp : Implementation of DLL Exports.


#include "stdafx.h"
#include "resource.h"


// The module attribute causes DllMain, DllRegisterServer and DllUnregisterServer to be automatically implemented for you
[ module(dll, uuid = "{236F7700-39BA-4A0A-B1EE-8745FF258516}", 
		 name = "eyetracker2_template", 
		 helpstring = "eyetracker2_template 1.0 Type Library",
		 resource_name = "IDR_EYETRACKER2_TEMPLATE") ]
class Ceyetracker2_templateModule
{
public:
// Override CAtlDllModuleT members
};
		 
